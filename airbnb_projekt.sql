﻿CREATE EXTENSION postgis;

-- tworzenie nowych kolumn dot. populacji
ALTER TABLE nyc_census_blocks 
	ADD COLUMN w_pop NUMERIC(5, 2),
	ADD COLUMN b_pop NUMERIC(5, 2),
	ADD COLUMN n_pop NUMERIC(5, 2),
	ADD COLUMN o_pop NUMERIC(5, 2),
	ADD COLUMN a_pop NUMERIC(5, 2);


-- statystyki dotyczace populacji	
UPDATE nyc_census_blocks SET 
 w_pop=100 * (popn_white)/(popn_total),
 b_pop=100 * (popn_black)/(popn_total),
 n_pop=100 * (popn_nativ)/(popn_total),
 a_pop=100 * (popn_asian)/(popn_total),
 o_pop=100 * (popn_other)/(popn_total)
 where popn_total<>'0';
-- dodanie statystyk do warstwy końcowej
Select  a.*,b.w_pop,b.b_pop,b.n_pop,b.o_pop,b.a_pop
into nyc_airbnb from ab_nyc_2019 a
left join nyc_census_blocks b on st_intersects(a.geom, b.geom);

-- tworzenie geometrii  buffor 1 km od punktów noclegowych
ALTER TABLE ab_nyc_2019 
	ADD COLUMN geomb geometry;
UPDATE ab_nyc_2019 SET 
 geomb=ST_Buffer(ab_nyc_2019.geom,1000,'quad_segs=8');


-- policzenie przestepstw zasiegu 1000 m
ALTER TABLE nyc_airbnb
    ADD COLUMN dangers INTEGER;
Update nyc_airbnb 
set dangers=d.danger from (SELECT ab_nyc_2019.gid as id,
       SUM(CASE WHEN st_contains((ab_nyc_2019.geomb),nyc_homicides.geom) THEN 1 ELSE 0 END) AS danger
FROM  ab_nyc_2019, nyc_homicides
GROUP BY ab_nyc_2019.gid
) as d
WHERE  nyc_airbnb.gid=d.id;


--- nabliższa stacja i dystans do niej 
ALTER TABLE nyc_airbnb
    ADD COLUMN station VARCHAR,
    ADD COLUMN station_dis NUMERIC(7, 2);
Update nyc_airbnb
set (station,station_dis)= 
(Select sb.station, sb.distance
from(SELECT
  ab_nyc_2019.gid as id,
  subway.name as station,
  subway.dist as distance
 FROM ab_nyc_2019
CROSS JOIN LATERAL 
  (SELECT
      name, 
      ST_Distance(ab_nyc_2019.geom,  nyc_subway_stations.geom) as dist
      FROM nyc_subway_stations
      ORDER BY ab_nyc_2019.geom <-> nyc_subway_stations.geom
     LIMIT 1
   ) AS subway) as sb
where nyc_airbnb.gid=sb.id);


--- najbliższa ulica 
ALTER TABLE nyc_airbnb
    ADD COLUMN street VARCHAR;
Update nyc_airbnb
set (street)=
(Select  st.street
from (SELECT 
  ab_nyc_2019.gid as id,
  street.name as street
 FROM ab_nyc_2019
CROSS JOIN LATERAL 
  (SELECT 
      name, 
      ST_Distance(ab_nyc_2019.geom,  nyc_streets.geom) as dist
      FROM nyc_streets
      Where name is not NULL
      ORDER BY ab_nyc_2019.geom <-> nyc_streets.geom
      

     LIMIT 1) as street)as st
where nyc_airbnb.gid=st.id);

